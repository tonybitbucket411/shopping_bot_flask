# coding: utf-8
import sys
import datetime
import traceback

from . import celery
from celery.signals import worker_shutdown, worker_process_shutdown
from redis_cache import SimpleCache

import settings
from common import get_domain_by_url
from common.mongo import results_db
import shopping_bot

results_cache = SimpleCache(expire=settings.API.CACHE_EXPIRE,
                           host=settings.API.CACHE_REDIS_HOST,
                           port=settings.API.CACHE_REDIS_PORT,
                           db=settings.API.CACHE_REDIS_DB)

@celery.task(bind=True)
def get_facets(task, url):
    shop_domain = get_domain_by_url(url)
    task_id = task.request.id
    try:
        cache_key = 'facets.%s' % url
        if cache_key in results_cache and not results_cache.isexpired(cache_key):
            result = results_cache.get_json(cache_key)
            cache_used = True
        else:
            result = shopping_bot.get_facets(task, url)
            results_cache.store_json(cache_key, result)
            cache_used = False
        results_db.insert_one({
            'task_id': task_id,
            'type': 'facets',
            'url': url,
            'domain': shop_domain,
            'result': result,
            'date': datetime.datetime.utcnow(),
            'cache_used': cache_used
        })
        return result
    except Exception:
        ex_type, ex, tb = sys.exc_info()
        error = traceback.format_exc(tb)
        results_db.insert_one({
            'task_id': task_id,
            'type': 'facets',
            'url': url,
            'domain': shop_domain,
            'error': 'error',
            'date': datetime.datetime.utcnow(),
        })
        raise


@celery.task(bind=True)
def make_order(task, products, user):
    task_id = task.request.id
    shop_domain = get_domain_by_url(products[0]['url'])
    try:
        result = shopping_bot.make_order(task, products, user)
        results_db.insert_one({
            'task_id': task_id,
            'type': 'make_order',
            'products': products,
            'domain': shop_domain,
            'result': result,
            'date': datetime.datetime.utcnow(),
        })
        return result
    except Exception:
        ex_type, ex, tb = sys.exc_info()
        error = traceback.format_exc(tb)
        results_db.insert_one({
            'task_id': task_id,
            'type': 'make_order',
            'domain': shop_domain,
            'error': error,
            'date': datetime.datetime.utcnow(),
        })
        raise


@worker_shutdown.connect
def worker_sd(*args, **kwargs):
    shopping_bot.Driver.close()
    print 'worker shutdown', args, kwargs,


@worker_process_shutdown.connect
def worker_pr_sd(*args, **kwargs):
    shopping_bot.Driver.close()
    print 'worker pr shutdown', args, kwargs,
