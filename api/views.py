# coding: utf-8
from collections import defaultdict

from flask import request
from flask import jsonify
from flask import json
from cerberus import Validator, ValidationError
from celery import states

from . import app
from . import celery
from shopping_bot.utils import CustomTaskStates, OrderConfirmation
from common import get_domain_by_url

from api import tasks

validator = Validator(allow_unknown=True)

facet_schema = {
    'products': {'type': 'list', 'schema': {
        'type': 'dict', 'schema': {
            'url': {'type': 'string', 'required': True},
        }
    }}}

products_schema = {
    'products': {'type': 'list', 'required': True, 'schema': {
        'type': 'dict', 'schema': {
            'id': {'type': 'integer', 'required': True},
            'url': {'type': 'string', 'required': True},
            'quantity': {'type': 'integer', 'required': True},
            'facets': {'type': 'dict'}
        }
    }}}

user_schema = {
    'user': {'type': 'dict', 'required': True, 'schema':  {
        # basic info
        'fullname': {'type': 'string', 'required': True},
        'first_name': {'type': 'string', 'required': True},
        'last_name': {'type': 'string', 'required': True},
        'email': {'type': 'string', 'required': True},
        'password': {'type': 'string', 'required': True},
        # address
        'address': {'type': 'dict', 'schema': {
            'first_name': {'type': 'string', 'required': True},
            'last_name': {'type': 'string', 'required': True},
            'fullname': {'type': 'string', 'required': True},
            'address_1': {'type': 'string', 'required': True},
            'address_2': {'type': 'string', 'required': True},
            'city': {'type': 'string', 'required': True},
            'state': {'type': 'string', 'required': True},
            'zip_code': {'type': 'integer', 'required': True},
            'country': {'type': 'string', 'required': True},
            'phone_number': {'type': 'string', 'required': True},
        }},
        'billing_address': {'type': 'dict', 'nullable': True, 'schema': {
            'first_name': {'type': 'string', 'required': True},
            'last_name': {'type': 'string', 'required': True},
            'fullname': {'type': 'string', 'required': True},
            'address_1': {'type': 'string', 'required': True},
            'address_2': {'type': 'string', 'required': True},
            'city': {'type': 'string', 'required': True},
            'state': {'type': 'string', 'required': True},
            'zip_code': {'type': 'integer', 'required': True},
            'country': {'type': 'string', 'required': True},
            'phone_number': {'type': 'string', 'required': True},
        }},
        # card
        'card': {'type': 'dict', 'schema': {
            'number': {'type': 'number', 'required': True},
            'cvv': {'type': 'number', 'required': True},
            'name': {'type': 'string', 'required': True},
            'year': {'type': 'integer', 'required': True},
            'month': {'type': 'integer', 'required': True},
        }},
    }}
}


class TaskStatus(object):
    READY = 'READY'
    UNREADY = 'UNREADY'
    FAILURE = 'FAILURE'


def get_task_result(task_id):
    task = celery.AsyncResult(task_id)
    print task, task.state, task.status, task.info
    if task.state == states.SUCCESS:
        return {
            'status': TaskStatus.READY,
            'data': task.result,
            'extra': task.info
        }
    if task.state == CustomTaskStates.WAIT_CONFIRM:
        return {
            'status': TaskStatus.READY,
            'data': task.info
        }
    if task.state in states.PROPAGATE_STATES:
        result = {'status': TaskStatus.FAILURE}
        if app.debug:
            result['error'] = str(task.traceback)
        return result
    return {'status': TaskStatus.UNREADY}


@app.route('/facets', methods=['POST'])
def make_facets_req():
    data = request.get_json()
    print 'new facets req', data
    valid = validator(data, facet_schema)
    if not valid:
        raise ValidationError(validator.errors)
    facets_tasks = {}
    for product in data['products']:
        url = product['url']
        task = tasks.get_facets.delay(url)
        facets_tasks[url] = task.task_id
    return jsonify(facets_tasks)


@app.route('/facets/<string:task_id>', methods=['GET'])
def get_facets_req(task_id):
    result = get_task_result(task_id)
    return json.dumps(result)


@app.route('/preview_orders', methods=['POST'])
def preview_order_req():
    data = request.get_json()
    print 'new preview req', data
    valid = validator(data, products_schema)
    if not valid:
        raise ValidationError(validator.errors)
    valid = validator(data, user_schema)
    if not valid:
        raise ValidationError(validator.errors)
    products_by_shops = defaultdict(list)
    for product in data['products']:
        domain = get_domain_by_url(product['url'])
        products_by_shops[domain].append(product)
    results = {}
    user = data['user']
    user['billing_address'] = user.get('billing_address', None)
    for shop, products in products_by_shops.items():
        task = tasks.make_order.delay(products=products, user=data['user'])
        results[shop] = {
            'task_id': task.task_id,
            'items_ids': [pr['id'] for pr in products]
        }
    return jsonify(results)


@app.route('/preview_orders/<string:task_id>', methods=['GET'])
def get_preview_order_req(task_id):
    result = get_task_result(task_id)
    return json.dumps(result)


@app.route('/confirm_orders/<string:task_id>', methods=['POST'])
def confirm_order_req(task_id):
    if not OrderConfirmation.check_wait(task_id):
        return json.dumps({'error': 'Order is expired'}), 400
    OrderConfirmation.confirm(task_id)
    return json.dumps({'confirmed': task_id})


if __name__ == '__main__':
    app.run()
