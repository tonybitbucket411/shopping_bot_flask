# coding: utf-8
import os

class API(object):
    DEBUG = True
    SENTRY_DSN = 'http://c59fc1b2a8974336ad62aedbe6ff804e:5be6688407f14160a9ac8b8b2ff5d4bf@ample.afindinc.com:10000/6'

    #celery settings
    CELERY_BROKER_URL = 'redis://localhost'
    CELERY_RESULT_BACKEND = 'redis://localhost/1'
    CELERY_ACKS_LATE = False
    CELERYD_PREFETCH_MULTIPLIER = 1
    CELERY_TASK_RESULT_EXPIRES = 7*24*60*60  # 7 days
    CACHE_REDIS_HOST = 'localhost'
    CACHE_REDIS_PORT = 6379
    CACHE_REDIS_DB = 4
    CACHE_EXPIRE = 30*60 # seconds

class MANAGER(object):
    DEBUG = True
    SENTRY_DSN = 'http://e8b17634a7a04c049b078dc9f4a95297:3813d78c380b44429cb8ff991285c036@ample.afindinc.com:10000/7'
    API_LOGIN = 'manager'
    API_PASSWORD = 'yAZ3xMPU'

class BOT(object):
    MONGO_HOST = 'localhost'
    MONGO_PORT = 27017
    SCENARIOS_DB = 'scenarios'
    RESULTS_DB = 'results'
    BROWSER_DRIVER = 'firefox'
    WINDOW_SIZE = (1280, 720)
    ORDER_CONFIRMATION_WAIT = 10*60  # seconds
    CONFIRMATION_CHECK_DELAY = 3  # seconds
    SCREENSHOT_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'screenshot')
    CONFIRM_ORDER_REDIS_HOST = 'localhost'
    CONFIRM_ORDER_REDIS_PORT = 6379
    CONFIRM_ORDER_REDIS_DB = 3
    ANTIGATE_KEY = 'fb4495fa4148757e4eaacb132404d174'
