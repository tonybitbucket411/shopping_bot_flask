# coding: utf-8
import logging

from .base import *

try:
    from .local import *
except ImportError:
    logging.debug('local settings load failed')
