# coding: utf-8
from . import log

import tldextract
import urllib

def get_domain_by_url(url):
    parsed_uri = tldextract.extract(url)
    return parsed_uri.domain + '.' + parsed_uri.suffix


def affiliate_url(url):
    domain = get_domain_by_url(url)
    affiliate_map = {
        'sportsauthority.com': 'http://www.pjtra.com/t/S0BHSkxMQERGTEhJR0BIREVMRQ?url=',
        'modells.com': 'http://www.pjatr.com/t/TUJGRUtFSUJGSE5KS0lCRkhHTUVO?url=',
        'esportsonline.com': 'http://click.linksynergy.com/deeplink?id=Y6YiHUX3Gfw&mid=25038&murl=',
        'fanatics.com': 'http://www.shareasale.com/r.cfm?u=1227817&b=31196&m=7124&urllink='
    }
    affiliated_url = affiliate_map.get(domain)

    if affiliated_url:
        return '{}{}'.format(affiliated_url, urllib.quote(url))
    return url
