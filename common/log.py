# coding: utf-8
import structlog
import logging


formatter = logging.Formatter('{filename="%(filename)s", lineno=%(lineno)s, message=%(message)s}')
handler = logging.StreamHandler()
handler.setFormatter(formatter)
handler.setLevel(logging.DEBUG)
fh = logging.FileHandler('spam.log')
fh.setFormatter(formatter)
fh.setLevel(logging.DEBUG)
app_logger = logging.getLogger('ample')
app_logger.propagate = False
app_logger.addHandler(handler)
app_logger.setLevel(logging.DEBUG)
app_logger.addHandler(fh)

structlog.configure(
    processors=[
        structlog.stdlib.filter_by_level,
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.TimeStamper(fmt='iso'),
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.JSONRenderer()
    ],
    context_class=structlog.threadlocal.wrap_dict(dict),
    logger_factory=structlog.stdlib.LoggerFactory(),
    wrapper_class=structlog.stdlib.BoundLogger,
    cache_logger_on_first_use=True,
)