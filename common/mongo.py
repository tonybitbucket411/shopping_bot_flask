# coding: utf-8
from pymongo import MongoClient

import settings

client = MongoClient(settings.BOT.MONGO_HOST,
                     settings.BOT.MONGO_PORT,
                     connect=False,
                     connectTimeoutMS=1000,
                     serverSelectionTimeoutMS=1000)

db = client.get_database(settings.BOT.SCENARIOS_DB)
scenario_db = db.get_collection('scenario')
results_db = db.get_collection('results')
