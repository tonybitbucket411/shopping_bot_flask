function requestFacets(element) {
  var $el = $(element);
  var form = $el.parents('form');
  var productUrls = form.find('textarea[name="urls"]').val();
  var urls = [];
  $.each(productUrls.split(/\n/), function(index, url) {
    console.log(url);
    url = url.trim();
    if (url) {
      urls.push({url: url});
    }
  });
  var requestUrl = '//' + window.location.hostname + ':5000/facets';
  return $.ajax({
    type: "POST",
    url: requestUrl,
    data : JSON.stringify({products: urls}),
    contentType : 'application/json',
    beforeSend: function (request) {
      if (api_auth && api_auth['login']) {
        request.withCredentials = true;
        request.setRequestHeader(
          'Authorization',
          'Basic ' + btoa( api_auth['login'] + ':' + api_auth['password']));
      }
    }
  }).done(function(data) {
    console.log('done', data);
    $.each(data, function(url, task_id) {
      var facetsUrl = requestUrl + '/' + task_id;
      window.open(facetsUrl, target='_blank');
    });

  }).fail(function(error) {
    console.log('error', error);
    alert('Submit facets error');
  });
}


function requestOrderPreview(element) {
  var $el = $(element);
  var form = $el.parents('form');
  var data = form.find('textarea[name="text"]').val();
  var requestUrl = '//' + window.location.hostname + ':5000/preview_orders';
  return $.ajax({
    type: "POST",
    url: requestUrl,
    data : data,
    contentType : 'application/json',
    beforeSend: function (request) {
      if (api_auth && api_auth['login']) {
        request.withCredentials = true;
        request.setRequestHeader(
          'Authorization',
          'Basic ' + btoa( api_auth['login'] + ':' + api_auth['password']));
      }
    }
  }).done(function(data) {
    console.log('done', data);
    $.each(data.shops, function(url,data) {
      var facetsUrl = requestUrl + '/' + data.taskId;
      window.open(facetsUrl, target='_blank');
    });

  }).fail(function(error) {
    console.log('error', error);
    alert('Submit order preview error');
  });
}

function requestOrderConfirm(element) {
  var $el = $(element);
  var form = $el.parents('form');
  var taskId = form.find('input[name="task_id"]').val();
  var requestUrl = '//' + window.location.hostname + ':5000/confirm_orders/' + taskId;
  return $.ajax({
    type: "POST",
    url: requestUrl,
    data : JSON.stringify({url: taskId}),
    contentType : 'application/json',
    beforeSend: function (request) {
      if (api_auth && api_auth['login']) {
        request.withCredentials = true;
        request.setRequestHeader(
          'Authorization',
          'Basic ' + btoa( api_auth['login'] + ':' + api_auth['password']));
      }
    }
  }).done(function(data) {
    console.log('done', data);
    var url = '//' + window.location.hostname + ':5000/preview_orders/' + taskId;
    window.open(url, target='_blank');

  }).fail(function(error) {
    console.log('error', error);
    alert('Submit order preview error');
  });
}
