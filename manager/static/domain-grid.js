function postGridData(data) {
  return $.ajax({
    type: "POST",
    url: '/domains/' + domain + '/scenario/' + scenario_type,
    data: JSON.stringify(data),
    contentType: 'application/json'
  });
}

gridController = {
  loadData: function (filter) {
    return $.ajax({
      type: "GET",
      url: '/domains/' + domain + '/scenario/' + scenario_type,
      dataType: "json"
    }).fail(function () {
      alert('Scenario failed to load');
    });
  },
  updateItem: function () {
    var data = getGridData();
    postGridData(data).fail(function () {
      alert('Scenario failed to update');
    });
  },
  deleteItem: function (item) {
    var data = getGridData();
    var i = data.indexOf(item);
    if (i != -1) {
      data.splice(i, 1);
    }
    postGridData(data).fail(function () {
      alert('Scenario failed to delete');
    });
  }
};

gridEl = $("#scenarioGrid");

function clickRowCopy(event) {
  console.log(event);
}

grid_fields.push({
  type: "control",
  modeSwitchButton: false,
  editButton: false,
  deleteButton: true,                             // show delete button
  clearFilterButton: false                        // show clear filter button
});
grid_fields.unshift({
  type: "control",
  modeSwitchButton: false,
  editButton: false,
  itemTemplate: function (value, item) {
    console.log('add item', value, item);
    return $("<button>").attr("type", "button").text("Copy")
      .on('click', function (event) {
        event.stopPropagation();
        event.preventDefault();
        var cell = $(event.target);
        var row = cell.parents('tr');
        var value = _.cloneDeep(row.data('JSGridItem'));
        gridEl.jsGrid("insertItem", value);
      })
  },
  headerTemplate: function () {
    return $("<button>").attr("type", "button").text("Add")
      .on("click", function () {
        console.log('add click');
        gridEl.jsGrid("insertItem", {});
      });
  }
});
gridEl.jsGrid({
  height: "100%",
  width: "100%",
  filtering: false,
  editing: true,
  sorting: false,
  autoload: true,
  controller: gridController,
  deleteConfirm: "Do you really want to delete this step?",
  fields: grid_fields
});

var $gridData = $('#scenarioGrid .jsgrid-grid-body tbody');

function getGridData() {
  return $.map($gridData.find("tr"), function (row) {
    return $(row).data("JSGridItem");
  });
}

$gridData.sortable({
  update: function (e, ui) {
    var items = getGridData();
    postGridData(items).fail(function () {
      alert('Scenario failed to update');
    });
    console && console.log("Reordered items", items);
  }
});

