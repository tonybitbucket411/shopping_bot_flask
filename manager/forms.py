# coding: utf-8
from wtforms import Form
from wtforms.validators import DataRequired
from wtforms import fields
from wtforms import validators

from shopping_bot import utils


class DomainMetadataForm(Form):
    select_quantity = fields.SelectField(choices=(
        (utils.SelectQuantityTypes.ByScenario, 'run qty scenario'),
        (utils.SelectQuantityTypes.RepeatAddToCart, 'repeat add to cart'),),
    )
    phone_format = fields.SelectField(choices=(
        (utils.PhoneFormatTypes.WithCountryCode, 'with country code'),
        (utils.PhoneFormatTypes.WithoutCountryCode, 'without country code'),),
    )
    year_format = fields.SelectField(choices=(
        (utils.YearFormatTypes.Full, 'full, 2015'),
        (utils.YearFormatTypes.Short, 'short, 15'),
        ),
    )
    facets_select_delay = fields.IntegerField(default=0, validators=[validators.Optional()])
    use_parent_selector = fields.BooleanField(default=False, validators=[validators.Optional()])

