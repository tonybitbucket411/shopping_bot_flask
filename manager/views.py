# coding: utf-8
from copy import deepcopy

from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
from flask import json
import pymongo

from common.mongo import scenario_db
from common.mongo import results_db
import settings

from .core import app
from shopping_bot.utils import BotCommands, FacetTypes, FetchLabelTypes
from .services import ScenarioTypes
from . import forms

scenario_facets_fields = [
    {'name': 'title', 'type': 'text'},
    {'name': 'fetch_label', 'type': 'select', 'valueField': 'id', 'textField': 'name',
     'items': [
         {'name': 'not', 'id': FetchLabelTypes.Not, },
         {'name': 'by css selector', 'id': FetchLabelTypes.BySelector, },
         {'name': 'label value list', 'id': FetchLabelTypes.LabelValuesList, },
         {'name': 'adjacent sibling after label', 'id': FetchLabelTypes.AdjacentSibling, },
     ]},
    {'name': 'type', 'type': 'select', 'valueField': 'id', 'textField': 'name',
     'items': [
         {'name': 'button', 'id': FacetTypes.Button, },
         {'name': 'select', 'id': FacetTypes.Select, },
         {'name': 'list', 'id': FacetTypes.ItemsList, },
         {'name': 'child images', 'id': FacetTypes.ChildImage, },
         {'name': 'combined images btns', 'id': FacetTypes.CombinedImageButton, },
     ]},
    {'name': 'selector', 'type': 'text'},
    {'name': 'label_sel', 'type': 'text'},
    {'name': 'values_sel', 'type': 'text'},
    {'name': 'description', 'type': 'text'}
]

scenario_enter_value_fields = [
    {'name': 'type', 'type': 'select', 'valueField': 'id', 'textField': 'name',
     'items': [
         {'name': 'enter text from value', 'id': BotCommands.EnterText},
         {'name': 'click button', 'id': BotCommands.ClickBtn, },
         {'name': 'delay (sec)', 'id': BotCommands.DelaySec, },
         {'name': 'open new url (url in selector)', 'id': BotCommands.OpenUrl},
         {'name': 'select month by value', 'id': BotCommands.SelectMonth},
         {'name': 'select year by value', 'id': BotCommands.SelectYear},
         {'name': 'select option', 'id': BotCommands.SelectOption},
         {'name': 'select state', 'id': BotCommands.SelectState},
         {'name': 'find element', 'id': BotCommands.FindElement},
         {'name': 'not find element', 'id': BotCommands.NotFindElement},
         {'name': 'refind elem enter text', 'id': BotCommands.RefindEnterText},
         {'name': 'enter captcha (captcha sel in value)', 'id': BotCommands.EnterCaptcha},
         {'name': 'click button by value (selector)', 'id': BotCommands.ClickBtnByValue},
         {'name': 'set element value', 'id': BotCommands.SetElementValue},
         {'name': 'enter phone number', 'id': BotCommands.EnterPhoneNumber},
         {'name': 'execute javascript', 'id': BotCommands.ExecuteJavaScript},
         {'name': 'select any from list', 'id': BotCommands.SelectFromList},
         {'name': 'switch to iframe', 'id': BotCommands.SwitchToIframe},
         {'name': 'switch to default', 'id': BotCommands.SwitchToDefault},
         {'name': 'custom command', 'id': BotCommands.CustomCommand},
     ]
     },
    {'name': 'selector', 'type': 'text', },
    {'name': 'value', 'type': 'text', },
    {'name': 'condition', 'type': 'text', },
    {'name': 'required', 'type': 'checkbox', },
    {'name': 'result', 'type': 'text', },
    {'name': 'description', 'type': 'text', }
]


def get_api_auth():
    return {
        'login': settings.MANAGER.API_LOGIN,
        'password': settings.MANAGER.API_PASSWORD,
    }


@app.route('/')
def index():
    domains = [d['domain'] for d in scenario_db.find({}, {'domain': 1})]
    api_auth = get_api_auth()
    return render_template('index.html', domains=domains, api_auth=api_auth)


@app.route('/domains/', methods=['POST'])
def add_domain():
    domain = request.form['domain']
    scenario_db.insert_one({
        'domain': domain
    })
    return redirect(url_for('index'))


@app.route('/domains/<string:domain>/<string:scenario_type>')
def view_domain(domain, scenario_type):
    if scenario_type == ScenarioTypes.Facets:
        fields = scenario_facets_fields
    elif scenario_type in ScenarioTypes.ALL:
        fields = scenario_enter_value_fields
    else:
        raise NotImplementedError
    return render_template('domain.html', domain=domain, fields=fields, scenario_type=scenario_type)


@app.route('/domains/<string:domain>/scenario/<string:scenario_type>')
def view_domain_scenario(domain, scenario_type):
    if scenario_type not in ScenarioTypes.ALL:
        raise ValueError
    domain_data = scenario_db.find_one({'domain': domain},
                                       {'_id': False, scenario_type: True})
    scenario = domain_data.get(scenario_type, [])
    return json.dumps(scenario)


@app.route('/domains/<string:domain>/scenario/<string:scenario_type>', methods=['POST'])
def update_domain_scenario(domain, scenario_type):
    scenario = request.json
    if scenario_type not in ScenarioTypes.ALL:
        raise ValueError
    result = scenario_db.update_many({'domain': domain},
                                     {'$set': {scenario_type: scenario}})
    return json.dumps({'modified_count': result.modified_count,
                       'matched_count': result.matched_count})


@app.route('/domains/<string:domain>/metadata/', methods=['GET', 'POST'])
def view_domain_metadata(domain):
    domain_data = scenario_db.find_one({'domain': domain},
                                       {'_id': False, 'metadata': True})
    domain_data = domain_data.get('metadata', {})
    form = forms.DomainMetadataForm(request.form, data=domain_data)
    print 'current data', domain_data
    if request.method == 'POST' and form.validate():
        domain_data.update(request.form.to_dict())
        print 'update metadata', domain, domain_data
        result = scenario_db.update_many({'domain': domain},
                                         {'$set': {'metadata': domain_data}})
    return render_template('metadata.html', form=form)


@app.route('/errors')
def get_errors():
    errors = results_db.find({}).sort('date', pymongo.DESCENDING)
    return render_template('errors.html', errors=errors)
