# coding: utf-8
from flask import Flask
from raven.contrib.flask import Sentry

import settings

app = Flask(__name__)
app.config.from_object(settings.MANAGER)
Sentry(app, logging=True)
