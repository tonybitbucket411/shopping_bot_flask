# coding: utf-8
from exceptions import ValueError


class DomainNotSupported(ValueError):
    pass
