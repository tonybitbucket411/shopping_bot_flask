# coding: utf-8

from common.mongo import scenario_db
from common import get_domain_by_url
from . import errors


class ScenarioTypes(object):
    Facets = 'facets'
    AddToCart = 'add-to-cart'
    MakeOrder = 'make-order'
    ConfirmOrder = 'confirm-order'
    SelectQuantity = 'select-quantity'
    CheckAvailability = 'check-product-availability'

    ALL = {Facets, AddToCart, MakeOrder, ConfirmOrder, SelectQuantity, CheckAvailability}


def get_scenario_by_url(scenario_type, url, allow_empty=False):
    domain = get_domain_by_url(url)
    domain_data = scenario_db.find_one({'domain': domain}, {'_id': False,
                                                            scenario_type: True})
    scenario = domain_data and domain_data.get(scenario_type, [])
    if not scenario and not allow_empty:
        raise errors.DomainNotSupported(domain)
    return scenario


def get_scenario_by_items(scenario_type, items, allow_empty=False):
    domain = None
    for item in items:
        item_domain = get_domain_by_url(item['url'])
        if domain and item_domain != domain:
            raise ValueError('Different domains per preview %s - %s' % (domain, item_domain))
        domain = item_domain
    return get_scenario_by_url(scenario_type, items[0]['url'], allow_empty)


def get_domain_metadata(url):
    domain = get_domain_by_url(url)
    domain_data = scenario_db.find_one({'domain': domain}, {'_id': False,
                                                            'metadata': True})
    metadata = domain_data and domain_data.get('metadata', [])
    if not metadata:
        raise errors.DomainNotSupported(domain)
    return metadata
