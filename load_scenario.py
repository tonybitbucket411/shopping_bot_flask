# coding: utf-8
import sys
import json
import datetime
from common.mongo import scenario_db

data = file(sys.argv[1]).read()
data = json.loads(data)
scenario_db.remove({})
scenario_db.insert_many(data)
