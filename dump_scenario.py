# coding: utf-8
import json
import datetime
from common.mongo import scenario_db

res = list(scenario_db.find({}, {'_id': False}))
f = file('scenario_' + str(datetime.datetime.now()) + '.json', 'w')
json.dump(res, f)
f.close()

#data = file('./scenario_2015-10-28 17:13:16.297606.json').read()
#data = json.loads(data)
#scenario_db.insert_many(data)