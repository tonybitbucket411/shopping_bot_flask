# coding: utf-8
import re
from time import sleep
from collections import defaultdict

from selenium.webdriver import ActionChains
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException

from . import utils


def group_by_len(lst, n):
    """group([0,3,4,10,2,3], 2) => [(0,3), (4,10), (2,3)]

    Group a list into consecutive n-tuples. Incomplete tuples are
    discarded e.g.

    >>> group(range(10), 3)
    [(0, 1, 2), (3, 4, 5), (6, 7, 8)]
    """
    return zip(*[lst[i::n] for i in range(n)])


class ActionTypes(object):
    GetFacets = 'get_facets'
    SelectFacets = 'select_facets'
    SelectQty = 'select_quantity'


def element_has_class(element, css_class):
    return css_class in element.get_attribute("class")


class Select2Helper(object):
    def __init__(self, driver, element):
        self.driver = driver
        self.element = element
        self.browser = element.parent

    def is_open(self):
        return element_has_class(self.element, 'select2-dropdown-open')

    def click(self):
        click_element = (ActionChains(self.browser)
                         .click_and_hold(self.element)
                         .release(self.element))
        click_element.perform()

    def open(self):
        if not self.is_open():
            self.click()

    def close(self):
        if self.is_open:
            self.click()

    def dropdown(self):
        return utils.find_elements(self.driver, '#select2-drop.select2-drop-active')[0]

    def get_items_el(self):
        self.open()
        item_divs = self.dropdown().find_elements_by_css_selector(
            '.select2-results li.select2-result-selectable div.val')
        return item_divs

    def get_options(self):
        options = [div.text for div in self.get_items_el()]
        self.close()
        return options

    def select_option(self, text):
        for div in self.get_items_el():
            if div.text == text.strip():
                print 'click', div.text
                div.click()
                return True
        raise NoSuchElementException(text)

def esportonline_try_all_selectors(driver, options_groups, priority):
    label, values_cont = options_groups[0]
    remaining_groups = options_groups[1:]
    label_text = label.text
    select = values_cont.find_element_by_css_selector('select')
    options = select.find_elements_by_css_selector('option')
    xpath = utils.get_xpath(driver, select)
    result = defaultdict(dict)
    print 'label', label_text, 'options', options
    for option in options:
        value = option.get_attribute("value")
        if not value:
            continue
        result[label_text][value] = {
            'value': value,
            'text': option.text,
            'xpath': xpath,
            'priority': priority
        }
        if remaining_groups:
            print 'get child', value
            option.click()
            sub_result = esportonline_try_all_selectors(driver, remaining_groups, priority+1)
            for key, sub_values in sub_result.items():
                for sub_value_key, sub_value in sub_values.items():
                    result[key][sub_value_key] = sub_value
    return result



def get_facets_esportsonline(driver):
    from . import facets
    facets_res = {}
    priority = 0
    add_to_cart_facets = utils.find_elements(driver, '.add-to-cart dl')
    for facet_el in add_to_cart_facets:
        priority += 1
        label = facet_el.find_element_by_css_selector('dt label')
        label_text = ' '.join(label.text.split()).split(':')[0]
        select_opt = facet_el.find_element_by_css_selector('dd select')
        result = facets.get_facets_by_select(driver, [select_opt])
        print '5'
        if result:
            print '6'
            xpath = utils.get_xpath(driver, select_opt)
            print '7'
            facets_res[label_text] = {
                'title': label_text,
                'result': result,
                'selector': utils.get_xpath(driver, select_opt),
                'type': utils.FacetTypes.Select,
                'priority': priority
            }
    rows = utils.find_elements(driver, '#super-product-table tbody tr')
    row_facets = defaultdict(dict)
    for row_index, facet_row in enumerate(rows):
        print 'parse row', row_index, facet_row
        sku_el = facet_row.find_element_by_css_selector('.display-sku')
        sku_label_text = sku_el.find_element_by_xpath('parent::*').text.split('\n')[0]
        row_facets['type'][sku_label_text] = {
            'text': sku_label_text,
            'row_xpath': utils.get_xpath(driver, facet_row),
            'row_index': str(row_index),
            'priority': priority,
        }
        options_cont = None
        try:
            options_cont = facet_row.find_element_by_css_selector('dl')
        except NoSuchElementException:
            print 'empty options cont'
        if options_cont:
            options = options_cont.find_elements_by_xpath('./*')
            options_group = group_by_len(options, 2)
            sub_res = esportonline_try_all_selectors(driver, options_group, priority + 1)
            for key, sub_values in sub_res.items():
                for sub_value_key, sub_value in sub_values.items():
                    xpath_indexes = row_facets[key].get(sub_value_key, {}).get('row_xpath', {})
                    xpath_indexes[str(row_index)] = sub_value['xpath']
                    print xpath_indexes
                    row_facets[key][sub_value_key] = sub_value
                    row_facets[key][sub_value_key]['row_xpath'] = xpath_indexes
    if row_facets:
        for key, sub_values in row_facets.items():
            values = sub_values.values()
            facets_res[key] = {
                'result': values,
                'selector': '',
                'title': key,
                'type': utils.FacetTypes.Select
            }
    return facets_res

def esportonline_select_qty_add_to_cart(driver, item):
    facets = list(sorted(item['facets'].values(), key=lambda f: int(f['option'].get('priority', 999))))
    if facets and facets[0]['option'].get('row_xpath'):
        # select specific product
        row_xpath = facets[0]['option']['row_xpath']
        qty_sel = row_xpath + '//input[@title="Qty"]'
        utils.fill_fields(driver, [qty_sel], item['quantity'])
    else:
        # one product on page
        utils.fill_fields(driver, ['#qty'], item['quantity'])
    sleep(2)
    utils.click_button(driver, ['.add-to-cart button.btn-cart'])


def select_facets_esportonline(driver, item):
    from .preview import logger

    logger.debug('custom select facets', item=item)
    driver.get(item['url'])
    sleep(10)  # wait until price load finished
    # TODO: use wait progress as indicator

    facets = list(sorted(item['facets'].values(), key=lambda f: int(f['option'].get('priority', 999))))
    row_index = None
    row_xpath = None
    for facet in facets:
        print 'next facet', facet
        if facet['option'].get('row_index', None) is not None:
            row_index = facet['option']['row_index']
            row_xpath = facet['option']['row_xpath']
            print 'find row index', row_index, row_xpath
            continue
        selector = facet['option']['xpath']
        print 'select facets', selector, row_xpath
        if row_xpath is not None:
            selector = row_xpath + facet['option']['row_xpath'][row_index]
        if not utils.select_by_option(driver, selector, facet['option']):
            raise ValueError


def select_facets_unique_sports(driver, item):
    from .preview import logger

    logger.debug('custom select facets', item=item)
    driver.get(item['url'])
    # select item facets
    sleep(1)
    for numb, facet in enumerate(item['facets'].values()):
        sel = facet['selector']
        tokens = re.split(r'(\[)(\d+)(\])', sel)
        index = int(tokens[-3]) + numb
        tokens[-3] = str(index)
        new_selector = ''.join(tokens)
        if not utils.select_by_option(driver, new_selector, facet['option']):
            raise ValueError
        sleep(10)


def get_facets_jet(driver):
    _jet_hide_country(driver)
    facets = {}
    variants = driver.find_elements_by_css_selector('.product.active .variant > .values')
    for var in variants:
        sel2_elem = var.find_element_by_xpath('./div[contains(@class, "select2-container")]')
        select_elem = var.find_element_by_xpath('./select')
        label_text = select_elem.get_attribute('name')
        sel2 = Select2Helper(driver, sel2_elem)
        options = sel2.get_options()
        if label_text and options:
            selector = '//*[@class="product active"]//div[@class="variant"]//select[@name="{}"]/../div'.format(label_text)
            facets[label_text] = {
                'title': label_text,
                'result': [],
                'selector': selector,
                'type': utils.FacetTypes.Select,
            }
            for value in options:
                facets[label_text]['result'].append({
                    'value': value,
                    'text': value,
                })
    return facets

def _jet_hide_country(driver):
    elms = utils.find_elements(driver, '.instruction_modal.invalid_country a.isSubmitButton', 3)
    if elms:
        elms[0].click()
    utils.click_button(driver, ['#mm_emailPromo .modal_close'], 1)

def select_facets_jet(driver, item):
    from .preview import logger
    logger.debug('custom select facets', item=item)
    driver.get(item['url'])
    _jet_hide_country(driver)
    # select item facets
    for facet in item['facets'].values():
        sleep(3)
        sel2_elem = utils.find_elements(driver, facet['selector'])[0]
        sel2 = Select2Helper(driver, sel2_elem)
        sel2.select_option(facet['option']['text'])


def select_qty_jet(driver, item):
    _jet_hide_country(driver)
    for _ in range(1, item['quantity']):
        sleep(0.1)
        inc_qty_btn = utils.find_elements(driver, '.product.active .qty.active .buttons a.plus')[0]
        inc_qty_btn.click()
    utils.click_button(driver, ['.product.active .qty.active div.submit>a'])
    sleep(3)
    return True



def select_modells_cctype(driver, step, results, extra=None, metadata=None):
    card_type = utils.single_card_type(str(extra['user']['card']['number']))
    types = {
        "VISA": '#cctypeVI > input',
        'AmericanExpress': '#cctypeAX > input',
        'Discover': '#cctypeDS > input',
        'MasterCard': '#cctypeMC > input',
    }
    selector = types[card_type]
    return utils.click_button(driver, [selector])


def get_facets_zappos(driver):
    from . import facets
    facets_res = {}
    facet_blocks = driver.find_elements_by_xpath('//*[@id="purchaseOptions"]/div[@class="dimension"]')
    for facet in facet_blocks:
        label_text = facet.get_attribute('id').split('-')[1].strip()
        try:
            sel2_elem = facet.find_element_by_xpath('.//select')
        except NoSuchElementException:
            continue
        result = facets.get_facets_by_select(driver, [sel2_elem])
        if result and label_text:
            result = [res for res in result if not res['value'].startswith('-1_')]
            facets_res[label_text] = {
                'title': label_text,
                'result': result,
                'selector': utils.get_xpath(driver, sel2_elem),
                'type': utils.FacetTypes.Select,
            }
    return facets_res


def select_qty_zappos(driver, item):
    stockId = None
    if item['quantity'] > 1:
        retry = 0
        add_to_fav = driver.find_element_by_xpath('//*[@id="favorites"]')
        hov = ActionChains(driver).move_to_element(add_to_fav)
        hov.perform()
        sleep(0.25)
        fav_link = add_to_fav.get_attribute('href')
        stockId = int(fav_link.split('=')[1])
    utils.click_button(driver, ['//*[@id="addToCart"]'])
    if stockId is not None:
        form_selector = '#stockId-{} td.qty  form'.format(stockId)
        form = driver.find_element_by_css_selector(form_selector)
        input_el = form.find_element_by_css_selector('input[name="changeQuantity"]')
        utils.enter_text_to_element(input_el, str(item['quantity']))
        update_btn = form.find_element_by_css_selector('button[type="submit"]')
        update_btn.click()
    return True


def get_facets_llbean(driver):
    from shopping_bot import facets
    form = driver.find_element_by_css_selector('form[name="product-form-data"]')
    item_buttons = []
    result = {}
    try:
        item_buttons = form.find_elements_by_css_selector('div.item-set div.item-label-radio')
    except NoSuchElementException:
        pass
    def get_facets_info():
        attrs = form.find_elements_by_css_selector('[data-container="attributes"] > [data-itemattribute]')
        for attr in attrs:
            label = attr.get_attribute('data-label')
            attr_values = []
            for input_value in attr.find_elements_by_css_selector('[data-attributeindex]'):
                if input_value.tag_name == 'select':
                    attr_type = utils.FacetTypes.Select
                    attr_values = facets.get_facets_by_select(driver, [input_value])
                else:
                    attr_type = utils.FacetTypes.Button
                    attr_values.append({
                        'value': input_value.get_attribute("value"),
                        'text': input_value.get_attribute("value"),
                    })
            if label in result:
                existing_values = []
                for itm in result[label]['result']:
                    existing_values.append(itm['value'])
                for itm in attr_values:
                    if itm['value'] not in existing_values:
                        result[label]['result'].append(itm)
            else:
                result[label]  = {
                    'title': label,
                    'result': attr_values,
                    'selector': utils.get_xpath(driver, attr)+'//',
                    'type': attr_type
            }
            for opt in result[label]['result']:
                opt['priority'] = 1
    if item_buttons:
        btn_res = []
        for item_btn in item_buttons:
            item_btn_el = item_btn.find_element_by_css_selector('input[type="radio"]')
            item_btn_el.click()
            label = item_btn.find_element_by_css_selector('label').text
            btn_res.append({
                'value': item_btn_el.get_attribute("value"),
                'text': label,
                'priority': 0,
                'xpath': utils.get_xpath(driver, item_btn_el)
            })
            get_facets_info()

        result['item'] = {
            'title': 'item',
            'result': btn_res,
            'selector': 'div.item-set div.item-label-radio',
            'type': utils.FacetTypes.Button,
        }
    else:
        get_facets_info()
    return result


def select_facets_llbean(driver, item):
    from .preview import prepare_btn_facet_css
    facets = list(sorted(item['facets'].values(), key=lambda f: int(f['option'].get('priority', 999)), reverse=True))
    print facets
    for facet in facets:
        facet_type = facet['type']
        if facet_type == utils.FacetTypes.Button:
            css = prepare_btn_facet_css(facet['selector'], facet['option'])
            print 'css', css
            if not utils.click_button(driver, [css]):
                raise ValueError
        elif facet_type == utils.FacetTypes.ItemsList:
            css = prepare_btn_facet_css(facet['selector'], facet['option'])
            if not utils.click_button(driver, [css]):
                raise ValueError
        elif facet_type == utils.FacetTypes.Select:
            selector = facet['selector']
            if facet['option'].get('xpath'):
                selector = facet['option']['xpath']
            if not utils.select_by_option(driver, selector, facet['option']):
                raise ValueError
        elif facet_type == utils.FacetTypes.ChildImage:
            css = prepare_btn_facet_css(facet['selector'], facet['option'])
            if not utils.click_button(driver, [css]):
                raise ValueError
        else:
            raise NotImplementedError
        sleep(0.5)


def select_facets_amazon(driver, item):
    from .preview import logger
    logger.debug('custom select facets', item=item)
    driver.get(item['url'])
    # select item facets
    for facet in item['facets'].values():
        facet_type = facet['type']
        if facet.get('vodd_sel', None):
            clicked1 = utils.click_button(driver, [facet['vodd_sel']])
            option_id = facet['option']['selector']
            clicked2 = utils.click_button(driver, [option_id])
            if not (clicked1 and clicked2):
                raise RuntimeError('%s Can not select facets %s' % (item['url'], str(facet)))
        elif facet_type == utils.FacetTypes.Select:
            selector = facet['selector']
            if not utils.select_by_option(driver, selector, facet['option']):
                raise RuntimeError('%s Can not select facets %s' % (item['url'], str(facet)))
        else:
            option_id = facet['option']['selector']
            clicked = utils.click_button(driver, [option_id])
            if not clicked:
                raise RuntimeError('%s Can not select facets %s' % (item['url'], str(facet)))
        sleep(1)


def get_facets_amazon(driver):
    from . import facets
    facets_res = {}
    facets_elems = driver.find_elements_by_css_selector('form#twister > div.a-section')
    for facet_div in facets_elems:
        facets_classes = facet_div.get_attribute('class').split()
        if 'vodd-dim-wrapper' in facets_classes:
            vodd_label = facet_div.find_element_by_css_selector(
                'span.dimension-button-label.dimension-display-text').text.strip()
            rows = facet_div.find_elements_by_css_selector('.vodd-table > tbody > tr')
            values = []
            for row in rows:
                text = (row
                        .find_element_by_css_selector('span.vodd-dimension-row-text.dimension-display-text')
                        .get_attribute('textContent')
                        .strip())
                row_selector = '#' + row.get_attribute('id')
                values.append({
                    'text': text,
                    'selector': row_selector
                })
            facets_res[vodd_label] = {
                'title': vodd_label,
                'result': values,
                'vodd_sel': '#' + facet_div.get_attribute('id'),
                'type': utils.FacetTypes.Select,
            }
        elif 'variation-dropdown' in facets_classes:
            label = facet_div.find_element_by_css_selector('.a-row label').text.strip()
            select_el = facet_div.find_element_by_css_selector('.a-dropdown-container select')
            result = [val for val in facets.get_facets_by_select(driver, [select_el]) if val['value'] != '-1']
            facets_res[label] = {
                'title': label,
                'result': result,
                'selector': '#' + select_el.get_attribute('id'),
                'type': utils.FacetTypes.Select,
            }
        else:
            label = facet_div.find_element_by_css_selector('.a-row label').text.strip()
            values = []
            for value_el in facet_div.find_elements_by_css_selector('ul.swatches > li'):
                span_label_el = utils.get_element(driver, utils.By.CSS_SELECTOR,
                                                  '.twisterTextDiv span', multiple=False, parent=value_el)
                img_label_el = utils.get_element(driver, utils.By.CSS_SELECTOR,
                                                 '.twisterImageDivWrapper > img', multiple=False, parent=value_el)
                img_label_el2 = utils.get_element(driver, utils.By.CSS_SELECTOR,
                                                  'span.a-button-thumbnail img', multiple=False, parent=value_el)
                if span_label_el:
                    label_text = span_label_el.text.strip()
                elif img_label_el:
                    label_text = img_label_el.get_attribute('alt').strip()
                else:
                    label_text = img_label_el2.get_attribute('alt').strip()
                values.append({
                    'text': label_text,
                    'selector': '#' + value_el.get_attribute('id'),
                })
            if not values:
                continue
            facets_res[label] = {
                'title': label,
                'result': values,
                'selector': '#' + facet_div.get_attribute('id'),
                'type': utils.FacetTypes.Button,
            }
    return facets_res



ACTIONS = {
    'unique-sports.com': {
        ActionTypes.SelectFacets:  select_facets_unique_sports
    },
    'esportsonline.com': {
        ActionTypes.GetFacets: get_facets_esportsonline,
        ActionTypes.SelectFacets: select_facets_esportonline,
        ActionTypes.SelectQty: esportonline_select_qty_add_to_cart
    },
    'jet.com': {
        ActionTypes.GetFacets: get_facets_jet,
        ActionTypes.SelectFacets: select_facets_jet,
        ActionTypes.SelectQty: select_qty_jet,
    },
    'zappos.com': {
        ActionTypes.GetFacets: get_facets_zappos,
        ActionTypes.SelectQty: select_qty_zappos,
    },
    'amazon.com' : {
        ActionTypes.GetFacets: get_facets_amazon,
        ActionTypes.SelectFacets: select_facets_amazon,
    }
}


CUSTOM_COMMANDS = {
    'select_modells_cctype': select_modells_cctype
}