# coding: utf-8
from time import sleep, time
import io
import os
import re
import uuid
import random

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
import redis
import structlog
import phonenumbers
from antigate import AntiGate
from PIL import Image
from io import BytesIO

import settings
from common import get_domain_by_url
from common.states import STATES_REVERT

logger = structlog.get_logger('ample.bot')

anti_gate = AntiGate(settings.BOT.ANTIGATE_KEY, send_config={'regsense': 1})


class FetchLabelTypes(object):
    Not = 'not'
    BySelector = 'by_selector'
    LabelValuesList = 'label_values_list'
    AdjacentSibling = 'adjacent_sibling'


class FacetTypes(object):
    Button = 'button'
    Select = 'select'
    ItemsList = 'items_list'
    ChildImage = 'child_image'
    CombinedImageButton = 'combined'



class BotCommands(object):
    DelaySec = 'delay_sec'
    ClickBtn = 'click_button'
    ClickBtnByValue = 'click_button_by_value'
    OpenUrl = 'open url'
    EnterText = 'enter_text_value'
    SelectMonth = 'select_month'
    SelectYear = 'select_year'
    SelectOption = 'select_option'
    SelectState = 'select_state'
    FindElement = 'find_element'
    NotFindElement = 'not_find_element'
    RefindEnterText = 'refind_enter_text'
    EnterCaptcha = 'enter_captcha'
    SetElementValue = 'set_element_value'
    EnterPhoneNumber = 'enter_phone_number'
    ExecuteJavaScript = 'execute_javascript'
    SelectFromList = 'select_from_list'
    SwitchToIframe = 'switch_to_iframe'
    SwitchToDefault = 'switch_to_default'
    CustomCommand = 'custom_command'


class SelectQuantityTypes(object):
    ByScenario = 'by_scenario'
    RepeatAddToCart = 'add_to_cart_repeat'


class PhoneFormatTypes(object):
    WithCountryCode = 'with_country_code'
    WithoutCountryCode = 'without_country_code'


class YearFormatTypes(object):
    Full = 'full'
    Short = 'short'


class CustomTaskStates(object):
    WAIT_CONFIRM = 'WAIT_CONFIRM'


class OrderConfirmation(object):
    key_tmpl = 'order_confirm.%s'
    wait_tmpl = 'order_wait.%s'
    redis_confirm = redis.StrictRedis(host=settings.BOT.CONFIRM_ORDER_REDIS_HOST,
                                      port=settings.BOT.CONFIRM_ORDER_REDIS_PORT,
                                      db=settings.BOT.CONFIRM_ORDER_REDIS_DB)

    @classmethod
    def check(cls, task_id):
        key = cls.key_tmpl % task_id
        return cls.redis_confirm.get(key)

    @classmethod
    def confirm(cls, task_id):
        # add expire
        key = cls.key_tmpl % task_id
        return cls.redis_confirm.set(key, time())

    @classmethod
    def set_wait(cls, task_id):
        logger.debug('set order wait', task_id=task_id, time=time())
        key = cls.wait_tmpl % task_id
        return cls.redis_confirm.set(key, time())

    @classmethod
    def check_wait(cls, task_id):
        key = cls.wait_tmpl % task_id
        res = cls.redis_confirm.get(key)
        logger.debug('check order wait', task_id=task_id, result=res)
        return res

    @classmethod
    def stop_wait(cls, task_id):
        key = cls.wait_tmpl % task_id
        return cls.redis_confirm.delete(key)



def check_order_confirmed(task_id):
    return


def confirm_order(task_id):
    pass


def get_new_screenshot_name(filename=None, ext='png'):
    if filename is not None:
        return os.path.join(settings.BOT.SCREENSHOT_PATH, filename) + '.' + ext
    file_path = None
    while not file_path or os.path.exists(file_path):
        file_path = os.path.join(settings.BOT.SCREENSHOT_PATH, uuid.uuid4().get_hex()) + '.' + ext
    return file_path


def get_page_source_name(filename=None, ext='html'):
    if filename is not None:
        return os.path.join(settings.BOT.SCREENSHOT_PATH, filename) + '.' + ext
    file_path = None
    while not file_path or os.path.exists(file_path):
        file_path = os.path.join(settings.BOT.SCREENSHOT_PATH, uuid.uuid4().get_hex()) + '.' + ext
    return file_path


def get_element(driver, by, selector, wait=0, multiple=True, parent=None):
    if parent is None:
        parent = driver
    try:
        if wait:
            return WebDriverWait(driver, wait).until(
                EC.presence_of_all_elements_located((by, selector))
            )
        else:
            if multiple:
                return parent.find_elements(by, selector)
            else:
                return parent.find_element(by, selector)
    except Exception as ex:
        logger.debug('get element error', by=by, selector=selector, wait=wait, exc=ex)
    if multiple:
        return []
    else:
        return None


def find_elements(driver, selector, wait=10):
    try:
        logger.debug("search elements", selector=selector, wait=wait)
        # if selector.find("[data-") > 0:
        #     a = selector.find("[")
        #     d = selector.find("=")
        #     c = selector.find("]")
        #     tag = selector[0:a]
        #     attr = selector[a + 1:d]
        #     val = selector[d + 1:c].replace('"', '')
        #     dt = selector[c + 1:]
        #     elms = driver.find_elements_by_tag_name(tag)
        #     logger.debug(tag, attr, val, len(elms))
        #     for e in elms:
        #         logger.debug(e.get_attribute(attr))
        #         if e.get_attribute(attr) == val:
        #             elems = e.find_elements_by_css_selector(dt)
        #             logger.debug(elems, dt)
        #             return elems

        if selector[0:2] == "//":
            return get_element(driver, By.XPATH, selector, wait)
        else:
            return get_element(driver, By.CSS_SELECTOR, selector, wait)
    except Exception as ex:
        logger.debug("can not find elems", exc=ex)
        return []


def click_button(driver, buttons, wait=10):
    # Click on the buttons from  the list of buttons
    try:
        for b in buttons:
            elems = find_elements(driver, b, wait)
            if len(elems) > 0:
                if elems[0].is_displayed() and elems[0].is_enabled():
                    logger.debug('click button', button=elems[0])
                    elems[0].click()
                    return True
    except WebDriverException as ex:
        logger.debug("click_button Exception" + str(ex))
    return False


def select_by_option(driver, selector, option):
    select_el = find_elements(driver, selector)
    if not select_el:
        return False
    select_el = select_el[0]
    select = Select(select_el)
    if option.get('value'):
        select.select_by_value(option['value'])
        return True
    elif option.get('text'):
        select.select_by_visible_text(option['text'])
        return True
    else:
        return False


def select_in_element(driver, selector, option):
    select_el = find_elements(driver, selector)
    if not select_el:
        return False
    select_el = select_el[0]
    if not select_el.is_displayed():
        script = "document.getElementById('%s').style.display='block'" % select_el.get_attribute('id')
        logger.debug('sel element visible', selecte_el=select_el, script=script)
        driver.execute_script(script)
    select = Select(select_el)
    option = str(option)
    try:
        select.select_by_value(option)
        return True
    except NoSuchElementException:
        pass
    try:
        select.select_by_visible_text(option)
        return True
    except NoSuchElementException:
        pass


def select_state(driver, selector, state):
    values = [state, state.lower(), state.upper(), STATES_REVERT[state], 'US/' + STATES_REVERT[state]]
    return select_from_list(driver, selector, values)



def select_calendar_value(driver, selector, value, year=True):
    select_el = find_elements(driver, selector)
    if not select_el:
        return False
    select_el = select_el[0]
    if not select_el.is_displayed():
        script = "document.getElementById('%s').style.display='block'" % select_el.get_attribute('id')
        logger.debug('sel element visible', selecte_el=select_el, script=script)
        driver.execute_script(script)
    value = int(value)
    select = Select(select_el)
    options = []
    if year:
        options.append(str(value))
        if value > 2000:
            options.append(str(value - 2000))
        else:
            options.append(str(value + 2000))
    else:
        options.append(str(value))
        if value < 10:
            options.append('%02d' % value)
    for option in options:
        try:
            select.select_by_value(option)
            return True
        except NoSuchElementException:
            pass
    for option in options:
        try:
            select.select_by_visible_text(option)
            return True
        except NoSuchElementException:
            pass
    logger.debug('can not select  calendar values', selector=selector, value=value, year=True)
    return False


def select_from_list(driver, selector, values):
    select_el = find_elements(driver, selector)
    if not select_el:
        return False
    select_el = select_el[0]
    if not select_el.is_displayed():
        script = "document.getElementById('%s').style.display='block'" % select_el.get_attribute('id')
        logger.debug('sel element visible', selecte_el=select_el, script=script)
        driver.execute_script(script)
    select = Select(select_el)
    for value in values:
        try:
            select.select_by_value(value)
            return True
        except NoSuchElementException:
            pass
        try:
            select.select_by_visible_text(value)
            return True
        except NoSuchElementException:
            pass
    logger.debug('can not select from list calendar values', selector=selector, values=values)
    return False




def enter_text_to_element(element, text):
    try:
        element.clear()
        text = str(text)
        for symb in text:
            element.send_keys(symb)
            sleep(random.uniform(0.01, 0.1))
        return True
    except WebDriverException as ex:
        logger.debug('enter text error', element=element, text=text, exc=ex)
        return False


def fill_fields(driver, fields, text):
    try:
        for field in fields:
            elem = find_elements(driver, field)
            if len(elem) > 0:
                return enter_text_to_element(elem[0], text)
    except WebDriverException as ex:
        logger.debug('fill field error', fields=fields, exc=ex)
    return False


def enter_phone_number(driver, selector, phone_number, phone_format):
    logger.debug('trying to enter phone number',
                 selector=selector, phone_number=phone_number, phone_format=phone_format)
    number = phonenumbers.parse(phone_number, None)
    if phone_format == PhoneFormatTypes.WithCountryCode:
        phone_number = phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
    elif phone_format == PhoneFormatTypes.WithoutCountryCode:
        phone_number = phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.NATIONAL)
    phone_number = phone_number.replace(' ', '').replace('(', '').replace(')', '').replace('-', '')
    return fill_fields(driver, [selector], phone_number)


def refind_element_fill_text(driver, selector, text):
    text = str(text)
    try:
        elems = find_elements(driver, selector)
        if not elems:
            logger.debug('fill text, can not find element', selector=selector, text=text)
            return False
        element = elems[0]
        last_entered = 0
        try:
            for symb in text:
                element.send_keys(symb)
                sleep(random.uniform(0.01, 0.1))
                last_entered += 1
            return True
        except StaleElementReferenceException:
            if last_entered:
                refind_element_fill_text(driver, selector, text[last_entered:])
            else:
                logger.debug('can not recheck enter, last entered=0', selector=selector, text=text)
                return False
    except WebDriverException as ex:
        logger.debug('recheck fill field error', selectors=selector, exc=ex)
    return False


def make_screenshot(driver, filename=None):
    path = get_new_screenshot_name(filename=filename)
    driver.get_screenshot_as_file(path)
    filename = os.path.split(path)[1]
    return filename


def get_shop_order_id(driver):
    shop_map = {
        'academy.com': {
            'xpath': '//*[@id="header_confirmation"]/h3[@class="ncConfirm_orderNum"]',
            'regex': r'(\d+)'
        },
        'jet.com': {
            'xpath': '//*[@id="page_content"]//p[contains(@class,"order_number")]',
            'regex': r'(\d+)'
        },
        'dickssportinggoods.com': {
            'xpath': '//body',
            'regex': r'order\W+#(\d+)'
        },
        'fanatics.com': {
            'xpath': '//body',
            'regex': r'your\W+order\W+number:.*(\d{2}-\d{5}-\d{10})'
        },
        'onlinesports.com': {
            'xpath': '//body',
            'regex': r'your\W+order\W+#\W+is:\W+(\w{2}\d+)'
        },
        'soccer.com': {
            'xpath': '//*[@id="CenterContent"]/center//*[@class="grandtotal"]',
            'regex': r'order\W+#(\d+)'
        },
        'sportchalet.com': {
            'xpath': '//*[@class="order-number"]/*[@class="value"]',
            'regex': r'(\d+)'
        },
        'sportsauthority.com': {
            'xpath': '//body',
            'regex': r'order\W+number:\W+(\d+)'
        },
        'unique-sports.com': {
            'xpath': '//*[@id="middle"]//p',
            'regex': r'(\d+)'
        },
        'zappos.com': {
            'xpath': '//body',
            'regex': r'order\W+number:\W+(\d+)'
        },
        'modells.com': {
            'xpath': '//*[@class="ml-checkout-accountSetUp-message"]',
            'regex': r'(\d+)'
        },
    }

    domain = get_domain_by_url(driver.current_url)

    if domain not in shop_map:
        logger.error('No instructions found for "{}"'.format(domain))
        return

    shop = shop_map[domain]

    try:
        text_str = driver.find_element_by_xpath(shop['xpath']).text
    except NoSuchElementException, e:
        logger.error("Can\'t get order ID container. - {} - {}".format(domain, e))
        return

    match_list = re.findall(shop['regex'], text_str, re.IGNORECASE | re.DOTALL)

    if not len(match_list):
        logger.error("Can\'t get order ID by regex. - {}".format(domain))
        return

    return match_list[0]


def save_page_source(driver, filename=None):
    path = get_page_source_name(filename=filename)
    with io.open(path, 'w', encoding='utf8') as f:
        f.write(driver.page_source)
    filename = os.path.split(path)[1]
    return filename


def get_text_by_captcha(driver, captcha_sel):
    captcha_el = find_elements(driver, captcha_sel)
    if not captcha_el:
        logger.debug('can not find captcha element', selector=captcha_sel)
        return
    captcha_el = captcha_el[0]
    box = (captcha_el.rect['x'],
           captcha_el.rect['y'],
           captcha_el.rect['x'] + captcha_el.rect['width'],
           captcha_el.rect['y'] + captcha_el.rect['height']
           )
    screen = driver.get_screenshot_as_png()
    im = Image.open(BytesIO(screen))
    region = im.crop(box)
    region.save('captcha_test.jpg', 'JPEG', optimize=True, quality=95)
    with BytesIO() as bio:
        region.save(bio, 'JPEG')
        data = bio.getvalue()
    captcha_id = anti_gate.send(data, binary=True)
    captcha_text = anti_gate.get(captcha_id)
    logger.debug('captcha recognized', text=captcha_text)
    return captcha_text


def single_card_type(number):
    number = str(number)
    if len(number) == 15:
        if number[:2] == "34" or number[:2] == "37":
            return 'AmericanExpress'
    if len(number) == 13:
        if number[:1] == "4":
            return "VISA"
    if len(number) == 16:
        if number[:4] == "6011":
            return "Discover"
        if int(number[:2]) >= 51 and int(number[:2]) <= 55:
            return "MasterCard"
        if number[:1] == "4":
            return "VISA"
        if number[:4] == "3528" or number[:4] == "3529":
            return "JCB"
        if int(number[:3]) >= 353 and int(number[:3]) <= 359:
            return "JCB"
    if len(number) == 14:
        if number[:2] == "36":
            return "DINERS"
        if int(number[:3]) >= 300 and int(number[:3]) <= 305:
            return "DINERS"
    return None


def card_type(number):
    types = {
        "VISA": ['VC', 'VI', "Visa", ],
        'AmericanExpress': ['AM', 'AE', 'American Express', 'AmericanExpress', 'American_Express'],
        'Discover': ['DC', 'DI', "Discover"],
        'MasterCard': ['MC', "MasterCard"],
        'JCB': ['JCB'],
        'DINERS': ['DINERS']
    }
    cardtype = single_card_type(number)
    card_types = types[cardtype]
    results = set()
    for value in card_types:
        results.update([value.lower(), value.upper(), value])
    return list(results)


def cc_full_date(month, year):
    if year > 2000:
        year -= 2000
    return '{:02d}/{:02d}'.format(int(month), int(year))


def format_year(year, year_format):
    if year_format == YearFormatTypes.Short:
        return year
    return year


def set_element_value(driver, selector, value):
    value = str(value)
    elems = find_elements(driver, selector)
    if not elems:
        logger.debug('set element value, can not find element', selector=selector, value=value)
        return False
    try:
        elem = elems[0]
        driver.execute_script("arguments[0].value = arguments[1];", elem, value)
        return True
    except Exception as e:
        logger.debug('set element value js execute error', selector=selector, value=value, error=e)
        return False


def execute_script(driver, script):
    logger.debug('execute script', script=script)
    return driver.execute_script(script)


def switch_to_iframe(driver, selector):
    logger.debug(' switch to iframe', selector=selector)
    iframe = find_elements(driver, selector)
    if not iframe:
        logger.debug('can not switch to iframe, frame not found', selector=selector)
        return
    iframe = iframe[0]
    driver.switch_to.frame(iframe)
    return True


def switch_to_default(driver):
    driver.switch_to.default_content()
    logger.debug('switch to default_content')
    return True

getXpath = """
getXPath = function (node) {
    if (node.id !== '') {
        return '//' + node.tagName.toLowerCase() + '[@id="' + node.id + '"]'
    }

    if (node === document.body) {
        return node.tagName.toLowerCase()
    }

    var nodeCount = 0;
    var childNodes = node.parentNode.childNodes;

    for (var i = 0; i < childNodes.length; i++) {
        var currentNode = childNodes[i];

        if (currentNode === node) {
            return getXPath(node.parentNode) +
                    '/' + node.tagName.toLowerCase() +
                    '[' + (nodeCount + 1) + ']'
        }

        if (currentNode.nodeType === 1 &&
                currentNode.tagName.toLowerCase() === node.tagName.toLowerCase()) {
            nodeCount++
        }
    }
};
return getXPath(arguments[0]);
"""

def get_xpath(driver, element):
    return driver.execute_script(getXpath, element)
