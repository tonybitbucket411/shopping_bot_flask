# coding: utf-8
import subprocess

from selenium import webdriver

import settings


def get_new_driver():
    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
    driver = webdriver.Firefox(firefox_profile=firefox_profile)
    driver.maximize_window()
    return driver


class Driver(object):
    driver = None
    display = None

    @classmethod
    def windows_size(cls):
        size = settings.BOT.WINDOW_SIZE
        if not size:
            return
        windows = subprocess.check_output(['xdotool', 'search', '--onlyvisible', 'firefox'])
        for window in windows.split():
            subprocess.call(['xdotool', 'windowsize', window.strip(), str(size[0]), str(size[1])])

    @classmethod
    def get_instance(cls):
        if cls.driver is None:
            cls.driver = get_new_driver()
            cls.windows_size()
        return cls.driver

    @classmethod
    def close(cls):
        if cls.driver is not None:
            cls.driver.close()
            cls.driver = None
        if cls.display is not None:
            cls.display.stop()
            cls.display = None
