# coding: utf-8
import structlog
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException

from manager.services import get_scenario_by_url, ScenarioTypes, get_domain_metadata, get_domain_by_url

from . import utils
from .utils import get_xpath
from .driver import Driver
from . import preview
from . import custom

logger = structlog.get_logger('ample.bot')

def _get_facets_by_child_image(driver, links):
    result = []
    if len(links):
        values = []
        for btn in links:
            values.append(btn.get_attribute("value"))
        values_same = all(values[0] == item for item in values)
        for link in links:
            img = link.find_element_by_tag_name('img')
            result.append({
                'src': img.get_attribute("src"),
                'alt': link.get_attribute("alt"),
                'title': link.get_attribute("title") or link.get_attribute("data-color"),
                'id': link.get_attribute("id"),
                'value': None if values_same else link.get_attribute("value"),
                'data-id': link.get_attribute("data-id"),
                'xpath': get_xpath(driver, link)
            })
    return result


def _get_facets_by_buttons(driver, buttons):
    result = []
    if len(buttons) > 0:
        values = []
        for btn in buttons:
            values.append(btn.get_attribute("value"))
        values_same = all(values[0] == item for item in values)
        for btn in buttons:
            text = ' '.join(btn.text.split())
            attrs = ['src', 'alt', 'title', 'id', 'value', 'data-id']
            if not any(btn.get_attribute(attr) for attr in attrs):
                continue
            result.append({
                       'src': btn.get_attribute("src"),
                       'alt': btn.get_attribute("alt"),
                       'title': btn.get_attribute("title"),
                       'id': btn.get_attribute("id"),
                       'value': None if values_same else btn.get_attribute("value"),
                       'data-id': btn.get_attribute("data-id"),
                       'text': text,
                       'tag': btn.tag_name,
                       'xpath': get_xpath(driver, btn)
            }
            )
    return result


def get_facets_by_select(driver, select):
    result = []
    if len(select) > 0:
        if not select[0].is_displayed():
            driver.execute_script(
                "document.getElementById('%s').style.display='block'" % select[0].get_attribute('id'))
        options = select[0].find_elements_by_xpath(".//option")
        for option in options:
            result.append(
                {
                    'value': option.get_attribute("value"),
                    'text': option.text,
                    'xpath': get_xpath(driver, select[0])
                }
            )
    else:
        logger.debug('select element not found')
    return result

def _get_facets_by_list(driver, items):
    result = []
    if len(items) > 0:
        for item in items:
            text = ' '.join(item.text.split())
            result.append({
                'text': text,
                'id': item.get_attribute("id"),
                'src': item.get_attribute("src"),
                'alt': item.get_attribute("alt"),
                'title': item.get_attribute("title"),
                'value': item.get_attribute("value"),
                'tag': item.tag_name,
                'xpath': get_xpath(driver, item)
            })
    return result


def check_availability(driver, scenario):
    results = {}

    for step in scenario:
        preview.exec_enter_command(driver, step, results)
        if results.get('available'):
            logger.debug('item available', step=step)
            return True
    else:
        logger.debug('item not available')
        return False

def get_values_by_items(driver, step_type, items):
    result = []
    if step_type == utils.FacetTypes.Button:
        result = _get_facets_by_buttons(driver, items)
    elif step_type == utils.FacetTypes.Select:
        result = get_facets_by_select(driver, items)
    elif step_type == utils.FacetTypes.ItemsList:
        result = _get_facets_by_list(driver, items)
    elif step_type == utils.FacetTypes.ChildImage:
        result = _get_facets_by_child_image(driver, items)
    elif step_type == utils.FacetTypes.CombinedImageButton:
        try:
            result = _get_facets_by_child_image(driver, items)
        except:
            pass
        if not result:
            result = _get_facets_by_buttons(driver, items)
            step_type = utils.FacetTypes.Button
        else:
            step_type = utils.FacetTypes.ChildImage
    else:
        raise ValueError('Unknown facet type')
    return result, step_type


def get_facets(task, url):
    task_id = task.request.id
    log = logger.new(task_id=task_id)
    log.info('New facets requests', url=url)
    domain = get_domain_by_url(url)
    scenario = get_scenario_by_url(ScenarioTypes.Facets, url)
    metadata = get_domain_metadata(url)
    availability_scenario = get_scenario_by_url(ScenarioTypes.CheckAvailability, url)
    driver = Driver.get_instance()
    driver.delete_all_cookies()
    driver.get(url)
    if custom.ACTIONS.get(domain, {}).get(custom.ActionTypes.GetFacets):
        action = custom.ACTIONS[domain][custom.ActionTypes.GetFacets]
        return action(driver)
    if not check_availability(driver, availability_scenario):
        return {
            '__error__': 'Item not available'
        }
    facets = {}
    for step in scenario:
        step_type = step['type']
        selector = step['selector']
        fetch_label = step.get('fetch_label', utils.FetchLabelTypes.Not)
        log.debug('run get facets', step=step)
        if fetch_label == utils.FetchLabelTypes.Not:
            items = utils.find_elements(driver, selector, 1)
            result, facet_type = get_values_by_items(driver, step_type, items)
            if result:
                log.debug('get btn facets', result=result)
                facets[step['title']] = {
                    'title': step['title'],
                    'result': result,
                    'selector': selector,
                    'type': facet_type,
                }
            else:
                log.debug('empty facets result')
        elif fetch_label == utils.FetchLabelTypes.BySelector:  # select label
            parents = utils.find_elements(driver, selector)
            logger.info('check fetch labels by selector, find parents', parents=parents)
            label_sel = step['label_sel']
            items_sel = step['values_sel']
            for parent in parents:
                try:
                    label = parent.find_element_by_xpath('.' + label_sel)
                    items = parent.find_elements_by_xpath('.' + items_sel)
                except NoSuchElementException:
                    logger.debug('can not find label/items in %s' % selector, parent=parent)
                    continue
                label_text = ' '.join(label.text.split()).split(':')[0]
                logger.debug('find label/values in %s' % selector, selector=selector, parent=parent, items=items,
                             label=label, label_text=label_text)
                result, facet_type = get_values_by_items(driver, step_type, items)

                if result:
                    log.debug('get facets', result=result)
                    result_selector = selector + ' ' + items_sel
                    if metadata.get('use_parent_selector'):
                        parent_xpath = get_xpath(driver, parent)
                        for option in result:
                            option['xpath'] = ''
                        result_selector = parent_xpath + items_sel
                    facets[label_text] = {
                        'title': label_text,
                        'result': result,
                        'selector': result_selector,
                        'type': facet_type,
                    }
                else:
                    log.debug('empty facets result')
        elif fetch_label == utils.FetchLabelTypes.LabelValuesList:
            container_elements = utils.find_elements(driver, selector)
            label_values_pairs = zip(*[iter(container_elements)]*2)
            logger.info('check fetch label by pairs, find in container', paris=label_values_pairs)
            for label, items_cont in label_values_pairs:
                label_text = ' '.join(label.text.split()).split(':')[0]
                items = items_cont.find_elements_by_xpath('./*')
                result, facet_type = get_values_by_items(driver, step_type, items)
                if result:
                    log.debug('find facets', label=label_text,  result=result)
                    facets[label_text] = {
                        'title': label_text,
                        'result': result,
                        'selector': get_xpath(driver, items_cont),
                        'type': facet_type,
                    }
                else:
                    log.debug('empty facets result')
        elif fetch_label == utils.FetchLabelTypes.AdjacentSibling:
            labels = utils.find_elements(driver, selector)
            logger.info('fetch adjacent sibling, find labels', labels=labels)
            sibling_sel = step['label_sel']
            items_sel = step['values_sel']
            for label in labels:
                items_cont = None
                items = None
                try:
                    items_cont_sel = 'following-sibling::' + sibling_sel + '[1]'
                    items_cont = label.find_element_by_xpath(items_cont_sel)
                    items = items_cont.find_elements_by_xpath('.' + items_sel)
                except NoSuchElementException:
                    logger.debug('can not find items in %s' % selector, label=label, items_cont=items_cont, items=items)
                    continue
                label_text = ' '.join(label.text.split()).split(':')[0]
                logger.debug('find label/values in %s' % selector, selector=selector, items=items,
                             label=label, label_text=label_text)
                result, facet_type = get_values_by_items(driver, step_type, items)

                if result:
                    log.debug('get facets', result=result)
                    facets[label_text] = {
                        'title': label_text,
                        'result': result,
                        'selector': get_xpath(driver, items_cont),
                        'type': facet_type,
                    }
                else:
                    log.debug('empty facets result')
        else:
            raise NotImplementedError('unknown fetch label type ' + fetch_label)
    return facets
