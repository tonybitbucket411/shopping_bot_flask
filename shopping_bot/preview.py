# coding: utf-8
import logging
from time import sleep, time

import structlog

from manager.services import get_scenario_by_items, ScenarioTypes, get_domain_metadata
from common import get_domain_by_url, affiliate_url
import settings

from . import utils
from . import custom
from .driver import Driver


logger = structlog.get_logger('ample.bot')


class PreviewStepTypes(object):
    ButtonClick = 'click_button'


def prepare_btn_facet_css(selector, option):
    if option.get('xpath'):
        return option['xpath']
    if selector[:2] == '//':
        xpath_element = option.get('tag', '*')
        attr_keys = ['id', 'value', 'alt']
        for attr_key in attr_keys:
            if option.get(attr_key):
                xpath_element += '[@%s="%s"]' % (attr_key, option[attr_key])
        if not xpath_element and option.get('text'):
            xpath_element += '[text()="%s"]' % option['text']
        return selector + xpath_element
    else:
        css_selector = ''
        if option.get('id'):
            css_selector += '#' + option['id']
        if option.get('title'):
            css_selector += '[title="' + option['title'] + '"]'
        if option.get('alt'):
            css_selector += '[alt="' + option['alt'] + '"]'
        if option.get('data-id'):
            css_selector += '[data-id="' + option['data-id'] + '"]'
        if not css_selector:
            raise NotImplementedError('css selector do not supported')
        return selector + css_selector

def exec_enter_command(driver, step, results, extra=None, metadata=None):
    if step.get('condition'):
        if not eval(step['condition'], locals(), globals()):
            logger.info('skip step by cond %s %s' % (str(step), str(results)))
            return
    step_type = step['type']
    res = None
    if step_type == utils.BotCommands.CustomCommand:
        res = custom.CUSTOM_COMMANDS[step['selector']](driver, step, results, extra, metadata)
    elif step_type == utils.BotCommands.ClickBtn:
        res = utils.click_button(driver, [step['selector']])
    elif step_type == utils.BotCommands.ClickBtnByValue:
        selector = eval(step['value'], locals(), globals())
        logger.debug('click button by value', selector=selector, value=step['value'])
        res = utils.click_button(driver, [selector])
    elif step_type == utils.BotCommands.DelaySec:
        sleep(int(step['selector']))
        res = True
    elif step_type == utils.BotCommands.OpenUrl:
        res = driver.get(step['selector'])
    elif step_type == utils.BotCommands.FindElement or step_type == utils.BotCommands.NotFindElement:
        find_delay = 10
        try:
            find_delay = eval(step['value'], locals(), globals())
        except:
            pass
        res = utils.find_elements(driver, step['selector'], find_delay)
        if step_type == utils.BotCommands.NotFindElement:
            res = not res
    elif step_type == utils.BotCommands.EnterText:
        text = eval(step['value'], locals(), globals())
        res = utils.fill_fields(driver, [step['selector']], text)
    elif step_type == utils.BotCommands.EnterPhoneNumber:
        text = eval(step['value'], locals(), globals())
        res = utils.enter_phone_number(driver, step['selector'], text, metadata['phone_format'])
    elif step_type == utils.BotCommands.RefindEnterText:
        text = eval(step['value'], locals(), globals())
        res = utils.refind_element_fill_text(driver, step['selector'], text)
    elif step_type == utils.BotCommands.SetElementValue:
        value = eval(step['value'], locals(), globals())
        res = utils.set_element_value(driver, step['selector'], value)
    elif step_type == utils.BotCommands.SelectMonth:
        month = eval(step['value'], locals(), globals())
        res = utils.select_calendar_value(driver, step['selector'], month, False)
    elif step_type == utils.BotCommands.SelectYear:
        year = eval(step['value'], locals(), globals())
        res = utils.select_calendar_value(driver, step['selector'], year, True)
    elif step_type == utils.BotCommands.SelectState:
        state = eval(step['value'], locals(), globals())
        res = utils.select_state(driver, step['selector'], state)
    elif step_type == utils.BotCommands.SelectFromList:
        values = eval(step['value'], locals(), globals())
        res = utils.select_from_list(driver, step['selector'], values)
    elif step_type == utils.BotCommands.SelectOption:
        option = eval(step['value'], locals(), globals())
        res = utils.select_in_element(driver, step['selector'], option)
    elif step_type == utils.BotCommands.EnterCaptcha:
        captcha_sel = step['value']
        captcha_text = utils.get_text_by_captcha(driver, captcha_sel)
        if captcha_text:
            res = utils.fill_fields(driver, [step['selector']], captcha_text)
    elif step_type == utils.BotCommands.ExecuteJavaScript:
        script = eval(step['value'], locals(), globals())
        res = utils.execute_script(driver, script)
    elif step_type == utils.BotCommands.SwitchToIframe:
        res = utils.switch_to_iframe(driver, step['selector'])
    elif step_type == utils.BotCommands.SwitchToDefault:
        res = utils.switch_to_default(driver)
    else:
        raise NotImplementedError(step_type)
    if step.get('result'):
        results[step['result']] = res
    logger.debug('exec_command', step_type=step_type, selector=step['selector'],
                 result=res, result_key=step.get('result'))
    if step.get('required') and not res:
        raise RuntimeError('Step is required' + str(step) + str(res))
    return res


def select_item_facets(driver, item, delay):
    sleep_delay = 0 if delay is None else float(delay)
    logger.debug('select facets', sleep_delay=sleep_delay)
    driver.get(affiliate_url(item['url']))
    # select item facets
    sleep(2)
    for facet in item['facets'].values():
        facet_type = facet['type']
        if facet_type == utils.FacetTypes.Button:
            css = prepare_btn_facet_css(facet['selector'], facet['option'])
            if not utils.click_button(driver, [css]):
                raise ValueError
        elif facet_type == utils.FacetTypes.ItemsList:
            css = prepare_btn_facet_css(facet['selector'], facet['option'])
            if not utils.click_button(driver, [css]):
                raise ValueError
        elif facet_type == utils.FacetTypes.Select:
            selector = facet['selector']
            if facet['option'].get('xpath'):
                selector = facet['option']['xpath']
            if not utils.select_by_option(driver, selector, facet['option']):
                raise ValueError
        elif facet_type == utils.FacetTypes.ChildImage:
            css = prepare_btn_facet_css(facet['selector'], facet['option'])
            if not utils.click_button(driver, [css]):
                raise ValueError
        else:
            raise NotImplementedError
        sleep(sleep_delay)
        logger.debug('sleep', sleep_time=sleep_delay)


def run_item_quantity_scenario(driver, item, quantity_scenario):
    logger.info('start qty %s', str(item))
    qty_results = {}
    for qty_step in quantity_scenario:
        exec_enter_command(driver, qty_step, qty_results, extra={'qty': item['quantity']})
        if qty_results.get('selected'):
            break
    else:
        if item['quantity'] != 1:
            raise ValueError('Can not select quantity')


def add_item_to_cart(driver, item, add_to_cart_scenario):
    results = {}
    for step in add_to_cart_scenario:
        exec_enter_command(driver, step, results)


def confirm_order(driver, scenario, user):
    results = {}
    for step in scenario:
        exec_enter_command(driver, step, results, extra={'user': user})
        if results.get('confirmed'):
            break
    else:
        raise RuntimeError('Can confirm order')


def make_order(task, items, user):
    task_id = task.request.id
    log = logger.new(task_id=task_id)
    log.info('New order requests %s, %s' % (str(items), str(user)))
    domain = get_domain_by_url(items[0]['url'])
    metadata = get_domain_metadata(items[0]['url'])  # HACK
    select_qty_by_scenario = metadata['select_quantity'] == utils.SelectQuantityTypes.ByScenario
    add_to_cart_scenario = get_scenario_by_items(ScenarioTypes.AddToCart, items)
    select_qty_scenario = get_scenario_by_items(ScenarioTypes.SelectQuantity, items, not select_qty_by_scenario)
    make_order_scenario = get_scenario_by_items(ScenarioTypes.MakeOrder, items)
    conform_order_scenario = get_scenario_by_items(ScenarioTypes.ConfirmOrder, items)
    driver = Driver.get_instance()
    driver.delete_all_cookies()
    try:
        for item in items:
            add_to_cart_repeat = 1 if select_qty_by_scenario else int(item['quantity'])
            for _ in range(add_to_cart_repeat):
                # select facets
                if custom.ACTIONS.get(domain, {}).get(custom.ActionTypes.SelectFacets):
                    action = custom.ACTIONS[domain][custom.ActionTypes.SelectFacets]
                    action(driver, item)
                else:
                    select_item_facets(driver, item, metadata.get('facets_select_delay', None))
                #select qty and add to cart
                if custom.ACTIONS.get(domain, {}).get(custom.ActionTypes.SelectQty):
                    action = custom.ACTIONS[domain][custom.ActionTypes.SelectQty]
                    action(driver, item)
                else:
                    if select_qty_by_scenario:
                        run_item_quantity_scenario(driver, item, select_qty_scenario)
                    add_item_to_cart(driver, item, add_to_cart_scenario)
        # make order
        results = {}
        for step in make_order_scenario:
            exec_enter_command(driver, step, results, extra={'user': user}, metadata=metadata)
    except:
        utils.make_screenshot(driver, task_id)
        utils.save_page_source(driver, task_id)
        raise
    filename = utils.make_screenshot(driver)
    logger.info('save order preview %s' % filename)
    complete_time = time()
    task.update_state(state=utils.CustomTaskStates.WAIT_CONFIRM, meta={
        'screenshot': filename,
        'order_id': task_id,
        'expire_at': complete_time + settings.BOT.ORDER_CONFIRMATION_WAIT,
    })
    logger.debug('order done, wait confirmation')
    utils.OrderConfirmation.set_wait(task_id)
    order_confirmed = utils.OrderConfirmation.check(task_id)
    while (time() - complete_time <= settings.BOT.ORDER_CONFIRMATION_WAIT) and not order_confirmed:
        logger.debug('wait', time=(time()-complete_time), title=driver.title)
        sleep(settings.BOT.CONFIRMATION_CHECK_DELAY)
        order_confirmed = utils.OrderConfirmation.check(task_id)
    utils.OrderConfirmation.stop_wait(task_id)
    if order_confirmed:
        logger.info('ORDER CONFIRMED %s' % task_id)
        successfully_complete = True
        shop_order_id = None
        try:
            confirm_order(driver, conform_order_scenario, user=user)
            shop_order_id = utils.get_shop_order_id(driver)
        except:
            successfully_complete = False
        confirm_filename = utils.make_screenshot(driver)
        return {
            'success': successfully_complete,
            'confirmed': True,
            'preview_screenshot': filename,
            'confirm_screenshot': confirm_filename,
            'order_id': task_id,
            'shop_order_id': shop_order_id
        }
    else:
        return {
            'confirmed': False,
            'preview_screenshot': filename,
            'order_id': task_id,
        }
